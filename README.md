# Demo pyspark

This is a mini framework to build jobs on pyspark

# Requirements

* python 2.7
* pyspark 1.6.3
* hadoop 2.6
* winutils (Hadoop 2.6.3)

# Environment variables
```
SPARK_HOME=C:\Path\To\spark-1.6.3-bin-hadoop2.6
HADOOP_HOME=C:\Path\To\winutils
```

# Winutils Instalation
Download `winutils.exe` and `hadoop.dll` from https://github.com/steveloughran/winutils/tree/master/hadoop-2.6.3/bin and copy to `C:\Path\To\winutils\bin` folder.
