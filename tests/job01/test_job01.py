# from jobs import job01
# from lib import job_test
#
#
# class TestJob01(job_test.JobTest):
#
#     def test_step1(self):
#         df_02 = self.job_dsl \
#             .add_table_from_list('tb_01', [("row1", 10), ("row2", 20), ("row3", 30)], ["name", "age"]) \
#             .do(job01.step1) \
#             .get_dataframe('df_02')
#         self.assertFrameEquals(df_02, self.job_dsl.create_dataframe_from_list(
#             [("row1", 10, 10.0), ("row2", 20, 10.0), ("row3", 30, 10.0)],
#             ["name", "age", "speed"]))
#
#     def test_step2(self):
#         df_03 = self.job_dsl \
#             .add_dataframe('df_02', [("row1", 10, 10.0), ("row2", 20, 10.0), ("row3", 30, 10.0)],
#                            ["name", "age", "speed"]) \
#             .do(job01.step2) \
#             .get_dataframe('df_03')
#         self.assertFrameEquals(df_03, self.job_dsl.create_dataframe_from_list([("row2", 20, 10.0), ("row3", 30, 10.0)],
#                                                                               ["name", "age", "speed"]))
#
#     def test_main(self):
#         self.job_dsl.add_table_from_list('tb_01', [("row1", 10), ("row2", 20), ("row3", 30)], ["name", "age"])
#         job01.run(self.job_dsl)
#         df_03 = self.job_dsl.get_dataframe('df_03')
#         self.assertFrameEquals(df_03, self.job_dsl.create_dataframe_from_list([("row2", 20, 10.0), ("row3", 30, 10.0)],
#                                                                               ["name", "age", "speed"]))
#
#
# if __name__ == '__main__':
#     job_test.main()
