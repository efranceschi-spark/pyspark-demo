from jobs import grupo_economico
from lib import job_test


class TestGrupoEconomico(job_test.JobTest):

    def test_reconcilia_grp_econm(self):
        reconcilia_grp_econm_df = self.job_dsl \
            .add_table_from_file('grup_ing.grp_econm', 'grupo_economico\\grp_econm.csv', '\t') \
            .do(grupo_economico.reconcilia_grp_econm) \
            .get_dataframe('reconcilia_grp_econm_df')
        self.assertFrameEquals(reconcilia_grp_econm_df, self.job_dsl.create_dataframe_from_list(
            [("row1", 10, 10.0), ("row2", 20, 10.0), ("row3", 30, 10.0)],
            ["name", "age", "speed"]))



    # def test_main(self):
    #     self.job_dsl.add_table('tb_01', [("row1", 10), ("row2", 20), ("row3", 30)], ["name", "age"])
    #     job01.run(self.job_dsl)
    #     df_03 = self.job_dsl.get_dataframe('df_03')
    #     self.assertFrameEquals(df_03, self.job_dsl.create_dataframe([("row2", 20, 10.0), ("row3", 30, 10.0)],
    #                                                                 ["name", "age", "speed"]))


if __name__ == '__main__':
    job_test.main()
