from pyspark.sql import Window
from pyspark.sql import functions as F
from pyspark.sql.dataframe import DataFrame


def transform(self, f):
    return f(self)


DataFrame.transform = transform

def run(hiveContext, df=None):
    def get_df_from_table(table_name):
        return hiveContext.table(table_name)


    def reconcilia_grp_econm(df):
        row_num_df = df.select('*', \
                            F.rowNumber().over( \
                                    Window.partitionBy('cgrp_econm') \
                                        .orderBy(F.desc('hextrc_hpu'))).alias('rnum'))
        return row_num_df.where((row_num_df.rnum > 1) &
                                (row_num_df.catulz_reg <> 'D') & 
                                (row_num_df.csit_grp_econm == 2)) \
                        .select('catulz_reg'     
                                ,'hextrc_hpu'
                                ,'cgrp_econm'
                                ,'ctpo_grp_econm'
                                ,'igrp_econm'
                                ,'csit_grp_econm'
                                ,'cstpo_grp_econm'
                                ,'dincl_reg'
                                ,'datulz_grp'
                                ,'dvaldd_grp'
                                ,'dult_alt_reg'
                                ,'hult_alt_reg'
                                ,'cusuar_senha'
                                ,'cidtfd_midia'
                                ,'dt_ingtao'
                                ,'nome_arq_ingtao'
                                ,'dt_ingtao_ptcao')


    def consolida_empr_grp_econm(df_empr_grp_econm):
        def inner(df):
            return df_empr_grp_econm.where(df_empr_grp_econm.ctpo_pssoa == 'F') \
                                    .join(df, df_empr_grp_econm.cgrp_econm == df.cgrp_econm) \
                                    .select(df_empr_grp_econm.ccgc_cpf_grp
                                            ,df_empr_grp_econm.cgrp_econm
                                            ,df_empr_grp_econm.ctpo_pssoa
                                            ,df.csit_grp_econm)
        return inner


    def trata_dominio_grp_econm(df):
        return df.distinct()\
                .select(df.ccgc_cpf_grp
                        ,F.when(df.cgrp_econm <> 0, 1).otherwise(0).alias('grupo_economico'))


    def join_base_inicial(df_base_inicial):
        def inner(df):
            return df_base_inicial.join(df, df_base_inicial.ccpf_cnpj.substr(1,9) == df.ccgc_cpf_grp, 'left') \
                                .select(F.concat(df_base_inicial.ccpf_cnpj.substr(1,9),F.lit('F')).alias('cd_cnpj_contraparte')
                                        ,F.when(df.grupo_economico == 1, 1).otherwise(0).alias('grupo_economico'))
        return inner
                

    def write_to_table(df, table_name):
        df.write.format('orc').mode('overwrite').saveAsTable(table_name)

    df_base_inicial = get_df_from_table('brain_wks.base_inicial_b38_completa')
    df_grp_econm = get_df_from_table('grup_ing.grp_econm')
    df_empr_grp_econm = get_df_from_table('grup_ing.empr_grp_econm') # TODO: alterar database para grup_csl

    def run(df_base_inicial,df_grp_econm,df_empr_grp_econm):        
        df_output = df_grp_econm\
                        .transform(reconcilia_grp_econm)\
                        .transform(consolida_empr_grp_econm(df_empr_grp_econm))\
                        .transform(trata_dominio_grp_econm)\
                        .transform(join_base_inicial(df_base_inicial))

        #write_to_table(df_output, 'sbx_brain.dataset_grup')
        return df_output
    return run(df_base_inicial,df_grp_econm,df_empr_grp_econm)
