from pyspark.sql import Window
from pyspark.sql import functions as F
from pyspark.sql.dataframe import DataFrame
from lib.job_dsl import JobDsl


def reconcilia_grp_econm(job_context):
    grp_econm_df = job_context.add_dataframe_from_table('grp_econm_df', 'grup_ing.grp_econm')
    row_num_df = grp_econm_df.select('*',
                                     F.rowNumber().over(
                                         Window.partitionBy('cgrp_econm').orderBy(F.desc('hextrc_hpu'))).alias('rnum'))
    reconcilia_grp_econm_df = row_num_df.where((row_num_df.rnum > 1) &
                                               (row_num_df.catulz_reg <> 'D') &
                                               (row_num_df.csit_grp_econm == 2)) \
        .select('catulz_reg'
                , 'hextrc_hpu'
                , 'cgrp_econm'
                , 'ctpo_grp_econm'
                , 'igrp_econm'
                , 'csit_grp_econm'
                , 'cstpo_grp_econm'
                , 'dincl_reg'
                , 'datulz_grp'
                , 'dvaldd_grp'
                , 'dult_alt_reg'
                , 'hult_alt_reg'
                , 'cusuar_senha'
                , 'cidtfd_midia'
                , 'dt_ingtao'
                , 'nome_arq_ingtao'
                , 'dt_ingtao_ptcao')
    job_context.add_dataframe('reconcilia_grp_econm_df', reconcilia_grp_econm_df)


def run(job_dsl):
    job_dsl \
        .do(step1)


if __name__ == '__main__':
    run(JobDsl())
