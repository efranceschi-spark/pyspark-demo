import pyspark.sql.functions as fn

from lib.job_dsl import JobDsl


def step1(job_context):
    df_01 = job_context.add_dataframe_from_table('df_01', 'tb_01')
    df_02 = df_01.withColumn("speed", fn.lit(10))
    job_context.add_dataframe('df_02', df_02)


def step2(job_context):
    df_02 = job_context.get_dataframe('df_02')
    df_03 = df_02.filter(df_02.age >= 18)
    job_context.add_dataframe('df_03', df_03)


def run(job_dsl):
    job_dsl\
        .do(step1)\
        .do(step2)


if __name__ == '__main__':
    run(JobDsl())
