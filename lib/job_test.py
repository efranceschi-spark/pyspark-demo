import unittest

from lib.job_dsl import TestJobDsl


class JobTest(unittest.TestCase):

    # noinspection PyPep8Naming
    def __init__(self, methodName='runTest'):
        super(JobTest, self).__init__(methodName)
        self.job_dsl = TestJobDsl()

    def assertFrameEquals(self, first, second):
        self.assertEqual(0, first.subtract(second).count())


main = unittest.TestProgram
