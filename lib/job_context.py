from pyspark import SQLContext
from pyspark.sql import DataFrame
from typing import Dict


class JobContext:

    sql_context = None  # type: SQLContext
    data_frames = None  # type: Dict[str, DataFrame]

    def __init__(self, spark_context, sql_context):
        self.sql_context = sql_context
        self.spark_context = spark_context
        self.data_frames = {}

    def add_dataframe(self, name, dataframe):
        self.data_frames[name] = dataframe
        return dataframe

    def add_dataframe_from_table(self, name, tablename):
        dataframe = self.sql_context.table(tablename)
        self.add_dataframe(name, dataframe)
        return dataframe

    def get_dataframe(self, name):
        return self.data_frames[name]


class TestJobContext(JobContext):

    tables = None  # type: Dict[str, DataFrame]

    def __init__(self, spark_context, sql_context):
        JobContext.__init__(self, spark_context, sql_context)
        self.tables = {}

    def add_table(self, tablename, data, schema):
        self.tables[tablename] = self.create_dataframe(data, schema)
        return self

    def add_dataframe_from_table(self, name, tablename):
        dataframe = self.tables[tablename]
        self.add_dataframe(name, dataframe)
        return dataframe
