from pyspark.context import SparkContext
from pyspark.sql.context import HiveContext
from lib.job_context import JobContext, TestJobContext
import csv
from typing import Dict


class JobDsl:
    # spark_context = SparkContext(master="local[*]", appName="job01_test")
    spark_context = SparkContext()  # TODO: Passar argumentos dinamicamente
    sql_context = None  # type: HiveContext
    job_context = None  # type: JobContext

    def __init__(self):
        self.sql_context = HiveContext(JobDsl.spark_context)
        self.job_context = JobContext(JobDsl.spark_context, self.sql_context)

    def __init__(self, sql_context, job_context):
        self.sql_context = sql_context
        self.job_context = job_context

    def add_dataframe(self, name, dataframe):
        self.job_context.add_dataframe(name, dataframe)
        return self

    def create_dataframe_from_list(self, data, schema):
        return self.sql_context.createDataFrame(data, schema)

    def create_dataframe_from_file(self, textfile, delimiter):
        with open(textfile, 'rb') as f:
            r = csv.reader(f, delimiter='|')
            all_data = list(r)
            schema = all_data[:1][0]
            data = all_data[1:]
        print schema
        print data
        return self.create_dataframe_from_list(data, schema)

    def get_dataframe(self, name):
        return self.job_context.get_dataframe(name)

    def do(self, function):
        function(self.job_context)
        return self


class TestJobDsl(JobDsl):

    test_job_context = None  # type: TestJobContext

    def __init__(self):
        JobDsl.__init__(self, HiveContext(JobDsl.spark_context), TestJobContext(JobDsl.spark_context, self.sql_context))
        # noinspection PyTypeChecker
        self.test_job_context = self.job_context

    def add_table_from_list(self, tablename, data, schema):
        self.test_job_context.tables[tablename] = self.create_dataframe_from_list(data, schema)
        return self

    def add_table_from_file(self, tablename, textfile, delimiter):
        self.test_job_context.tables[tablename] = self.create_dataframe_from_file(textfile, delimiter)
        return self

    # noinspection PyMethodOverriding
    def add_dataframe(self, name, data, schema):
        dataframe = self.create_dataframe_from_list(data, schema)
        self.job_context.add_dataframe(name, dataframe)
        return self
